<?php
namespace Common\Model;
use Think\Model;
class AttrModel extends CommonModel {
    public function ckvo($vo='')
    {
        if(!$vo) return;
        $vo['url_code'] = urlencode($vo['url']);
        return $vo;
    }
}    