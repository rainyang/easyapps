<?php
namespace Api\Controller;
use Think\Controller;
class IndexController extends CommonController {
    public function index(){
        $Apps = D('Apps');
        $map['id'] = $this->apid;
        $data = $Apps->where($map)->find();
        $this->ajaxReturn($data,$this->format);
    }
}
